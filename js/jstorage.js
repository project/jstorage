var JStorage = JStorage || function (namespace) {
  this.namespace = namespace;
};

JStorage.prototype.values = {};
JStorage.prototype.toStore = {};
JStorage.prototype.collecting = false;
JStorage.hasLocalStorage = typeof localStorage != 'undefined';
JStorage.hasSessionStorage = typeof sessionStorage != 'undefined';

JStorage.prototype.del = function(name, options) {
  options = this.prepareOptions(options, true);
  if (options.group) {
    var oldGroup = this.get(options.group);
    if (typeof oldGroup == 'undefined' || oldGroup === null) {
      return;
    }
    delete oldGroup[name];
    var oldGroupId = options.group;
    delete options.group;
    this.set(oldGroupId, oldGroup, options);
  }
  else {
    JStorage.cookie(name, null);
    if (JStorage.hasLocalStorage) {
      window.localStorage.removeItem(name);
    }
    if (JStorage.hasSessionStorage) {
      window.sessionStorage.removeItem(name);
    }
    if (typeof this.dbDelete != 'undefined') {
      if (this.collecting) {
        this.toStore[name] = null;
      }
      else {
        this.dbDelete(name);
      }
    }
    this.values[name] = null;
  }
};

JStorage.prototype.set = function(name, value, options) {
  options = this.prepareOptions(options, false);
  var storageName = options.group ? options.group : name;
  var toStore = $.toJSON(value);
  if (options.group) {
    var oldGroup = this.get(options.group);
    if (oldGroup === null || typeof oldGroup != 'object') {
      oldGroup = {};
    }
    oldGroup[name] = toStore;
    this.values[storageName] = oldGroup;
    toStore = $.toJSON(oldGroup);
  }
  else {
    this.values[storageName] = value;
  }
  if (options.profile && typeof this.dbSet != 'undefined') {
    if (this.collecting) {
      this.toStore[storageName] = toStore;
    }
    else {
      this.dbSet(storageName, toStore);
    }
  }
  if (options.browser) {
    if (options.session) {
      if (JStorage.hasSessionStorage) {
        window.sessionStorage[storageName] = toStore;
      }
      else {
        JStorage.cookie(storageName, toStore);
      }
    }
    else {
      if (JStorage.hasLocalStorage) {
        window.localStorage[storageName] = toStore;
      }
      else {
        JStorage.cookie(storageName, toStore, {expires: 9999});
      }
    }
  }
};

JStorage.prototype.get = function(name, options) {
  options = this.prepareOptions(options, true);
  var storageName = options.group ? options.group : name;
  if (storageName in this.values) {
    if (this.values[storageName] === null) {
      return null;
    }
    if (options.group) {
      return name in this.values[storageName] ? JStorage.stringToBool(this.values[storageName][name]) : null;
    }
    return this.values[storageName];
  }
  var toReturn = undefined;
  if (options.profile && typeof this.dbGet != 'undefined') {
    toReturn = this.dbGet(storageName);
    if (typeof toReturn == 'string') {
      toReturn = $.secureEvalJSON(toReturn);
      if (options.group) {
        if (typeof toReturn == 'object' && toReturn !== null && name in toReturn) {
          this.values[storageName] = JStorage.stringToBool(toReturn);
          return JStorage.stringToBool(toReturn[name]);
        }
        else {
          this.values[storageName] = null;
          return null;
        }
      }
      else {
        this.values[storageName] = JStorage.stringToBool(toReturn);
        return JStorage.stringToBool(toReturn);
      }
    }
  }
  if (options.browser) {
    if (JStorage.hasLocalStorage) {
      toReturn = window.localStorage.getItem(storageName);
    }
    if (typeof toReturn == 'undefined' || toReturn == 'undefined') {
      toReturn = JStorage.cookie(storageName);
    }
    if ((typeof toReturn == 'undefined' || toReturn == 'undefined') && JStorage.hasSessionStorage) {
      toReturn = window.sessionStorage.getItem(storageName);
    }
    if (typeof toReturn == 'string' && toReturn != 'undefined') {
      toReturn = $.secureEvalJSON(toReturn);
      if (options.group) {
        if (typeof toReturn == 'object' && toReturn !== null && name in toReturn) {
          this.values[storageName] = JStorage.stringToBool(toReturn);
          return JStorage.stringToBool(toReturn[name]);
        }
        else {
          this.values[storageName] = null;
          return null;
        }
      }
      else {
        this.values[storageName] = JStorage.stringToBool(toReturn);
        return JStorage.stringToBool(toReturn);
      }
    }
  }
  this.values[storageName] = null;
  return null;
};

JStorage.prototype.startCollecting = function() {
  this.collecting = true;
}

JStorage.prototype.stopCollecting = function() {
  this.collecting = false;
  if (typeof this.dbSet != 'undefined') {
    for (var i in this.toStore) {
      if (this.toStore[i] === null) {
        this.dbDelete(i);
      }
      else {
        this.dbSet(i, this.toStore[i]);
      }
    }
  }
  this.toStore = {};
}

JStorage.prototype.prepareOptions = function(options, getting) {
  if (typeof options == 'undefined') {
    options = {};
  }
  return {
    profile: 'profile' in options ? options.profile : true,
    browser: 'browser' in options ? options.browser : true,
    session: 'session' in options ? options.session : getting,
    group: 'group' in options ? options.group : false
  };
};

JStorage.stringToBool = function(toReturn) {
  if (toReturn == 'false')
    return false;
  if (toReturn == 'true')
    return true;
  if (toReturn == 'undefined')
    return undefined;
  if (toReturn == 'null')
    return null;
  return toReturn;
};

JStorage.prototype.dbSet = function(name, value) {
  if (Drupal.settings.jstorage.method == 2) {
    eval(Drupal.settings.jstorage.set + '(' + this.namespace + ',' + name + ',' + value + ');');
  }
  else {
    this.dbSetHelper(name, value);
  }
};

JStorage.prototype.dbGet = function(name) {
  if (Drupal.settings.jstorage.method == 2) {
    return eval(Drupal.settings.jstorage.get + '(' + this.namespace + ',' + name + ');');
  }
  else {
    return this.dbGetHelper(name);
  }
};

JStorage.prototype.dbDelete = function(name) {
  if (Drupal.settings.jstorage.method == 2) {
    eval(Drupal.settings.jstorage.del + '(' + this.namespace + ',' + name + ');');
  }
  else {
    this.dbDeleteHelper(name);
  }
};

JStorage.cookie = function(name, value, options) {
  // @author Klaus Hartl/klaus.hartl@stilbuero.de
  if (typeof value != 'undefined') { // name and value given, set cookie
    options = options || {};
    if (value === null) {
      value = '';
      options.expires = -1;
    }
    var expires = '';
    if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
      var date;
      if (typeof options.expires == 'number') {
        date = new Date();
        date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
      } else {
        date = options.expires;
      }
      expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
    }
    var path = '; path=/';
    var domain = options.domain ? '; domain=' + (options.domain) : '';
    var secure = options.secure ? '; secure' : '';
    document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
  }
  else { // only name given, get cookie
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
}