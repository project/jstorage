<?php

function jstorage_admin() {
  $form = array();
  $form['jstorage_method'] = array(
    '#title' => t('Storage method'),
    '#description' => t('Select your storage method. "%fast_drupal" requires that you have your jstorage module in sites/all/modules and $base_path defined in settings.php', array('%fast_drupal' => t('Fast Drupal'))),
    '#type' => 'radios',
    '#options' => array(t('Normal Drupal'), t('Fast Drupal'), t('External')),
    '#default_value' => variable_get('jstorage_method', 0),
  );
  $form['external_storage'] = array(
    '#title' => t('External storage'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#Collapsed' => variable_get('jstorage_method', 0) != 2,
  );
  $form['external_storage']['jstorage_external_set'] = array(
    '#title' => t('Save function'),
    '#type' => 'textfield',
    '#description' => t('Type in the name of the save function to be used. For instance "setUserProperty". It must take the parameters "namespace", "key", "value" in the given order.'),
    '#default_value' => variable_get('jstorage_external_set', 'setUserProperty'),
  );
  $form['external_storage']['jstorage_external_get'] = array(
    '#title' => t('Load function'),
    '#type' => 'textfield',
    '#description' => t('Type in the name of the load function to be used. For instance "getUserProperty". It must take the parameters "namespace", "key" in the given order.'),
    '#default_value' => variable_get('jstorage_external_get', 'getUserProperty'),
  );
  $form['external_storage']['jstorage_external_delete'] = array(
    '#title' => t('Delete function'),
    '#type' => 'textfield',
    '#description' => t('Type in the name of the delete function to be used. For instance "deleteUserProperty". It must take the parameters "namespace", "key" in the given order.'),
    '#default_value' => variable_get('jstorage_external_delete', 'deleteUserProperty'),
  );
  return system_settings_form($form);
}